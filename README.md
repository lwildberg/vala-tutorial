# Vala Documentation

This project currently gets deployed to: https://lwildberg.pages.gitlab.gnome.org/vala-tutorial/

## Requirements

- `sphinx >= 4.2.0`
- `furo` (Sphinx Theme)
- `sphinx-copy-button` (Sphinx Extension)
- `make`

## Getting started

1. Build the website with:

```sh
make
```

or if you are using GNOME builder, you can click on the "Build" button.

2. In the `build` directory, open a html file in your favourite web browser. `index.html` contains the home page.

3. Whenever you want to rebuild the site with changes you make in `source`, repeat step 1.

## References

- [Sphinx Documentation](https://www.sphinx-doc.org/en/master/contents.html)
- [Furo Theme](https://github.com/pradyunsg/furo)
- [reStructuredText(The format that the documentation is written in.)](https://www.writethedocs.org/guide/writing/reStructuredText/)