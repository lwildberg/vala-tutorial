Tooling
=======

Here you can find useful information about specific tools used to develop with Vala:

* Vala Language Server

* Vala Linter

* Vala templating tool