Developer Guides
================

You can learn here how writing programs with Vala works and find out more about certain features.

Getting started
---------------

* :doc:`Installation <guides/installation>`

* :doc:`Hello, World! <guides/helloworld>`


Basics
------

* :doc:`Variables <guides/variables>`

* :doc:`Data Types <guides/types>`

* :doc:`Operators <guides/operators>`

* :doc:`Functions <guides/functions>`

* :doc:`Comments <guides/comments>`

* :doc:`Conditional Execution <guides/conditional>`

* :doc:`Loops <guides/loops>`


Object-Oriented Programming
---------------------------

* :doc:`Overview <guides/oop>`

* :doc:`Classes <guides/classes>`

* :doc:`Structs <guides/structs>`

* Enumerations and Flags

* :doc:`Compact Classes <guides/compact-classes>`

* :doc:`Acess Modifiers <guides/access-modifiers>`

* :doc:`Methods <guides/methods>`

* :doc:`Constructors <guides/constructors>`

* :doc:`Properties <guides/properties>`

* :doc:`Signals <guides/signals>`

* Deriving

* :doc:`Alias'es <guides/aliases>`

* Interfaces


Code Organization
-----------------

* Project structure

* Introducing Meson

* Namespaces


Data Collections
----------------

* :doc:`Arrays <guides/arrays>`

* Lists

* Hashtables


Memory Management
-----------------

* References and values

* Ownership


Additional Concepts
-------------------

* Error handling

* Asynchronous methods

* Parameter directions

Advanced topics
---------------

* GTK integration

* DBUS integration

* Functions with variable length arguments

* Methods with Syntax Support

* Assertions and Contract Programming

* Preprocessor

* API documention

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Contents:

   guides/installation
   guides/helloworld
   guides/variables
   guides/types
   guides/operators
   guides/functions
   guides/comments
   guides/conditional
   guides/loops
   guides/oop
   guides/classes
   guides/structs
   guides/compact-classes
   guides/access-modifiers
   guides/methods
   guides/constructors
   guides/properties
   guides/signals
   guides/aliases
   guides/fields
   guides/arrays
