Hello, World!
=============

Write and run a Vala program
----------------------------

Type this code into a file named ``hello.vala``:

.. code-block:: vala

   int main (string[] args) {
       stdout.printf ("Hello, World!");
       return 0;
   }

Then go to the terminal in the directory of this file and compile the program.
This will result in an executable ``hello`` file.

.. code-block:: console

   valac hello.vala

To execute it, do:

.. code-block:: console

   ./hello

This should output:

.. code-block:: console

   Hello, World!

Explanation
-----------

First, we define a method, called ``main``:

.. code-block:: vala

   int main (string[] args) {

The ``main``-method is always the entrypoint for a Vala program.

Then inside of it, there is

.. code-block:: vala

   stdout.printf ("Hello, World!");

This calls ``printf``, a method, that prints text out on the commandline.

And finally the program returns ``0``, a value meaning success.

.. code-block:: vala

   return 0;


