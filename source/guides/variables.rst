Variables
=========

Variables are for storing data in your program, that you can then access later.

How to use
----------

A variable has a name and a type, which you need to declare befor use.

For example a variable named ``foo``, with the type ``int``:

.. code-block:: vala

   int foo;

Later you can assign values to it, for example ``20``:

.. code-block:: vala

   foo = 20;

You can also do both steps together, to make it simpler:

.. code-block:: vala

   int foo = 20;


Constants
---------

With the ``const`` modifier a variable can be turned into a constant:

.. code-block:: vala

   const int BAR = 13;

The value is assigned once to it and it will never change.
Constants are usually named `UPER_CASE`.

You can define constants also inside of classes, structs or other objects or namespaces.


Also useful
-----------

* If the type is already written elsewhere or so obvios, you can use ``var``
  instead of the type name. This can help simplifying your code.
  But be aware that using it too much makes your code less readable.

  .. code-block:: vala

     // instead of
     MainLoop main_loop = new MainLoop ();

     // you can write
     var main_loop = new MainLoop ();

* Also if you have multiple variables of the same type declared at the
  same location in your code, you can declare them altogether:

  .. code-block:: vala

     string foo, bar;

     string hello = "Hello", world = "World";

   
