Compact Classes
===============

Another option to create data types are compact classes, which are cheaper in
construction compared to regular classes. They are useful for cases in which
structs are not powerful enough and regular classes are overkill.

In compact classes, all the fields are private, but they can be accessed through
properties, ensuring encapsulation.

What features do compact classes support?
-----------------------------------------

* Custom constructors
* :doc:`aliases`
* Private :doc:`fields`
* :doc:`properties`

How to instantiate a compact class
----------------------------------

Instantiating a compact class is done in the same way a regular class is. Using
the ``new`` keyword and specifying the type wanted.

.. code-block:: vala

   MyClass instance; // A variable with the expected type
   instance = new MyClass ();

How to create your own compact class
------------------------------------

Declaring a compact class is done almost in the same way declaring a class is, with
the exception that the ``[Compact (opaque = true)]`` attribute is set.

.. code-block:: vala

   [Compact (opaque=true)
   public class MyClass {

   }

Properties in compact classes
-----------------------------

Using properties in compact classes is very similar to the ones in Object-derived
classes with the exception that they cannot be ``construct`` or ``set construct``.

.. code-block:: vala

   [Compact (opaque=true)]
   public class Person {
       public int age { get; set; default = 0; }
       public string name { get; set; default = ""; }
   }

Custom constructors
-------------------

You can create custom constructors to initialize a compact class with data:

.. code-block:: vala

   [Compact (opaque=true)]
   public class Person {
       public int age { get; set; default = 0; }
       public string name { get; set; default = ""; }

       public Person (int age, string name) {
           this.age = age;
           this.name = name;
       }
   }

Non-opaque compact classes
--------------------------

So far, only opaque compact classes have been covered. Non-opaque compact classes
are somewhat different to their opaque counterpart, with the following differences:

* Non-opaque compact classes can't have private fields, only public ones
* They can't use properties' default setters and getters as they can only have public fields

You can declare them like this:

.. code-block:: vala

   [Compact]
   public class MyClass {
       public int my_field; // Public field
       public int my_property { // Public property using public fields
           get {
               return my_field;
           }
           set {
               my_field = value;
           }
       }
   }

Reference counted compact classes
---------------------------------

By default, compact classes lack reference counting, but you can manually create
your own ref and unref methods and ask the compiler to use them.

.. code-block:: vala

   [Compact (opaque=true)]
   [CCode (ref_function="myclass_ref", unref_function="myclass_unref")]
   public class MyClass {
       private int ref_count = -1;
       public void my_method () {
           print ("Hello, from a compact class\n");
       }

       public unowned MyClass ref () {
           AtomicInt.add (ref ref_count, 1);
           return this;
       }

       public void unref () {
           if (AtomicInt.dec_and_test (ref ref_count)) {
               free ();
           }
       }

       private extern void free ();
   }

Also useful
-----------

* You can create aliases of compact classes: :doc:`aliases`.
* Compact classes can use multiple constructors: :doc:`constructors`.