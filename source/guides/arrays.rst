Arrays
======

For long lists of data, arrays can be used.

How to use
----------

First create a variable, that contains an array:

.. code-block:: vala

   int[] list = new int[10];

In this case the array has space for 10 variables of the type ``int``.

You can also already put data into the array on creation.
The size of it will be calculated from the data. In this case 5:

.. code-block:: vala

   int[] list = { 1, 2, 3, 4, 5 };

Then there can be values assigned to the arrays:
In the squared brackets you specify the index of the item in the array.
Be aware of that the index of the first element is 0:

.. code-block:: vala

   list[0] = 11;

In the same way the values can be read:

.. code-block:: vala

   stdout.printf ("%d\n", list[0]);

A slice is a part of an array cutted out.
For that the index of the start and of the end of the slice, separated by a ``:``, needs to be specified:

.. code-block:: vala

   int[] list = { 1, 2, 3, 4, 5 };

   int[] slice = list[0:3];

In this case the second list called "slice" would contain ``{ 1, 2, 3, 4 }``.

Also useful
-----------

* To get the length of an array conveniently, as specified on creation, ``.length ()`` can be used:

  .. code-block:: vala

     int[] list = new int[10];

     int list_size = list.length; // 10

