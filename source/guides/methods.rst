Methods
=======

Methods are similar to functions, but they work on an object.
They are defined in a class, struct, interface, compact class, enum or flag.

Defining a method
-----------------

As an example we use here a struct:

.. code-block:: vala

   public struct MyStruct {
       public int a;
       public int b;

       public int sum () {
           return this.a + this.b;
       }
   }

The method ``sum`` returns **int** here. But methods can also take parameters like functions.
In the method can be any code. You can access other members of the object or call methods.

Special keywords inside an object
---------------------------------

Inside a method you have a few keywords available to use in your code:

* **this**: With this keyword you have access to the current instance of the object the method is working on at the moment.
  You can access other members through it or even store a reference to the object somewhere else.

* **super ()**: This gives access to members of the class the current one was derived from.
  It is useful if a property or method was overwritten by a new, but you still want to access the old one.

Static methods
--------------

Static methods are methods that are defined inside an object, but actually don't work on one.
They are very similar to functions.

.. code-block:: vala

   public class MyClass : Object {

       public static void hi () {
           stdout.printf ("hi!\n");
       }
   }

   int main (string[] args) {
       MyClass.hi ();
       return 0;
   }

You can call them without having an instance by prefix them with the containing class.

Static methods can be used in every object type.

Virtual methods
---------------

We discussed already the more simple type of methods, but in classes and interfaces you can also have **virtual** methods.
Virtual methods are methods you can actually override in derived classes.

When you subclass, the non-virtual methods actually don't override the methods, they just hide them.
So virtual methods can instead be overridden, so that the code from the old class you dervied from also uses now your new method.

This can be useful for example when you have a class that provides a method where derived classes can override that with their own functionality.

.. code-block:: vala

   public class Printer : Object {
       public virtual void print (string s) {
           stdout.printf (s);
       }
   }

   public class ErrorPrinter : Printer {
       public override void print (string s) {
           stderr.printf (s);
       }
   }

   int main (string[] args) {
       Printer printer = new ErrorPrinter ();
       // prints to stderr, even though printer is of type Printer
       printer.print ("hello world");
       return 0;
   }

As written above, use the **virtual** keyword, to mark a method as virtual,
and the **override** keyword, to override it in a derived class.

Abstract methods
----------------

You can also let a method have no body, just the declaration,
so that derived classes are forced to override it. For that the whole class needs to be abstract.

.. code-block:: vala

   public abstract class Printer : Object {
       public abstract void print (string s);

       public void print_hello () {
           this.print (hello);
       }
   }

   public class ErrorPrinter : Printer {
       public override void print (string s) {
           stderr.printf (s);
       }
   }

   int main (string[] args) {
       Printer printer = new ErrorPrinter ();
       // prints hello to stderr
       printer.print_hello ();
       return 0;
   }
