Data Types
==========

Types describe what data a variable contains.

There are multiple simple types:

Integer
-------

Integers can hold whole numbers, like 1, 2, -10 or 30000:

.. code-block:: vala

   int number = 42;


String
------

For storing text you can use strings:

.. code-block:: vala

   string text = "Hello, Vala!";


Boolean
-------

A boolean can have only ``true`` or ``false`` as the value:

.. code-block::

   bool vala_is_great = true;


Floating Point Number
---------------------

Floating Point Numbers are numbers, that can have a point, so not only whole numbers:

.. code-block:: vala

   float pi = 3.141;

Also useful
-----------

..
   TODO

* For all builtin simple types you can look *here*.
