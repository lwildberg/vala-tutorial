Installation
============

.. _linux:

Linux
-----

Vala is available on a large variety of Linux distributions.
Mostly you want to install other development files for libraries, that you want to use with vala.

.. note::

   You should avoid long-term-support realeases, because they often have only very old versions of vala.

Fedora
~~~~~~

Development files usually come in ``*-devel`` packages, for example ``libgee-devel``.

.. code-block:: console

   sudo dnf install vala

Debian
~~~~~~

You need to install ``*-dev`` packages, to get development files on Debian.

.. code-block:: console

   sudo apt install valac

Arch Linux
~~~~~~~~~~

.. code-block:: console

   sudo pacman -S vala

\*BSD
-----

First you install the port:

.. code-block:: console

   cd /usr/ports/lang/vala/ && make install clean

And then you can add the package:

.. code-block:: console

   pkg install vala

Windows
-------

MSYS2
~~~~~

MSYS2 provides a Linux-like environment for Windows. First install `MSYS2 <https://www.msys2.org>`__,
then vala:

.. code-block:: console

   pacman -S mingw-w64-x86_64-gcc
   pacman -S mingw-w64-x86_64-pkg-config
   pacman -S mingw-w64-x86_64-vala

You also need to install all libraries you want to use individually.

Windows Subsystem for Windows (WSL)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install a Linux distribution in WSL and then go on with the :ref:`installation instructions for Linux <linux>`.

Mac OS X
--------

To install Vala on you can use `brew <https://brew.sh>`__, a package manager for OS X:

.. code-block:: console

   brew install vala

