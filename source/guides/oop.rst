Object Oriented Programming
===========================

Object Orientation is a concept where you basically model **objects** with your code.
These objects contain data and you also have code that works on these object.

In Vala you have already objects ready to use. For example strings or numbers.
Those objects are different and have each its own **type**.
The type defines what you can do with the object.

So in this section of the documentation you will learn how to create and use the objects,
but also how to create new types of objects.

Terminology
-----------

There are a few words which have a very specific meaning and you should know them:

* **Class**: A class describes a type, and how objects of it should look and how they behave.

* **Instance**: An instance is a not only a description, but a usable object
  that was created after the description of a class.

* **Inheritance**: (Or derivation) When you want to create a new class,
  sometimes you want to reuse an existing class and extend it with your functionality.
  This is called inheritance and your new class is now **inherited** from the existing one.

Concepts
--------

Object Orientation has lots of advantages. Here are a few written down:

* **Abstraction**: When you have complex code, you can just put it into a object,
  and then the complexity is abstracted away and the rest of your code doesn't need to
  bother about it anymore.

* **Encapsulation**: Because an object abstracts the internals of it, the rest of your code cannot see them.
  This makes your code much simpler, because changes internal to one object don't affect the rest.
  Also maybe some implementation details are private and nobody else should see them except the object itself.

* **Reusablilty**: When you put code into an object, other code even from other projects can reuse it easily.
  This makes this concept also very well suited for libraries. Another point is that you can derive a class and
  reuse its existing functionality without needing to write everything manually again.

Those are just a fraction of what makes object orientations nice.
Vala is a language designed with object orientation as the first paradigm in mind,
so it is highly recommended to write your code in Vala in an object oriented way.

There are a multiple options available to create new types. Each has its own use case.
The other pages in this section are giving more details about them.
They also explain the different features each option supports.