Comments
========

Commenting is a way of letting the compiler ignore some of your code.
This is useful for example for documenting.

How to use
----------

Vala allows comments in code in different ways:

.. code-block:: vala

   // Comment continues until end of line

   /* Comment lasts
   between
                  delimiters */


Also useful
-----------

* There are also special comments that get included in generated API documentation:

  Read more about it `here`.

  .. code-block:: vala

     /**
      * Documentation comment
      */

.. TODO


