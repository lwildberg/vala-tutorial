Aliases
=======

With aliases you can define new types, based on an existing one.
This works for structs, compact classes, enums and flags.

An alias has all the same fields, methods and properties as the type it derives from.
But additionally you can extend it with your own methods, constructors or constants:

How to create an alias
----------------------

.. code-block:: vala

   struct SpeakingPerson : Person {
       public void speak () {
           print ("My name is" + this.name + "\n");
       }
   }

After the name of the new alias a ``:`` and the name of the existing type is written.

How to use an alias
-------------------

The alias has the same functionalities as the original type.
You can instanciate and use it as usual.
