Access modifiers
================

Access modifiers specify how members of an object are exposed to other code.

There are 4 different modifiers. From least to most restrictive:

* **public** has no restrictions on accessibility. Everything is completely visible and modifyable.

* **internal** allows only code in the same package as the object access to it.
  This makes only sense in libraries, or when you compile your app first into different packages,
  that you then link later together.

* **protected** allows only access from inside the object or subtypes or aliases of the object to it.
  It is an a bit less restrictive version of **private**

* **private**: In this case something can be only accessed from code inside of the object.
  From other regions it is not visible and can therefore also not be changed.

.. warning::

   Some object types support only specific access modifier for certain members.
   For example structs can only have public fields.

.. warning::

   Often for fields and properties ``protected`` and ``internal`` are only hints for the Vala compiler to throw errors
   when you try to access them from other Vala code. But in the C API they appear as ``public``!
