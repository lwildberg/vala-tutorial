Signals
=======

**GLib.Object** derived classes can have signals.
With signals you can, as the name is already suggesting, emit events or messages from your object.
Users of your object can listen and react to them.

Signals look like methods, they have a return value and parameters, but they work in the reversed way.
Instead of the user of the object calling the signal, the object emits it and the user processes it.

How to connect to signals
-------------------------

If you have constructed an object, you can connect a function very easily to it.
In the example the signal looks like this:

.. code-block:: vala

   signal void test_signal (int c);

Connecting to this signal is then done this way:

.. code-block:: vala

   void handler (int c) {
       stdout.printf (@"a signal with value $c arrived");
   }

   object.test_signal.connect (handler);

Or with a lambda:

.. code-block:: vala

   object.test_signal.connect ((c) => {
       stdout.printf (@"a signal with value $c arrived");
   });

How to create signals
---------------------

You can add signals to your class like this:

.. code-block:: vala

   public class MyClass : Object {
       public signal int something ();
   }


How to emit a signal
--------------------

You can emit the signal then from other code by calling it similar to a method:

.. code-block:: vala

   public class MyClass : Object {
       public signal int something ();

       public void do_something () {
           this.something ();

           // ...
       }
   }

Signal "details"
----------------

Signals can optionally have a special parameter called the "detail".
Listeners of a signal can specify to only connect to emissions with a specific signal:

.. code-block:: vala

   object.test_signal["my-detail"].connect ((c) => {
       stdout.printf (@"a signal with value $c arrived");
   });

But note that the detail is optional. You can also connect to a signal and listen to all emissions, regardless of any detail.

You can emit a signal with a detail like this:

.. code-block:: vala

   public class MyClass : Object {
       public signal int something ();

       public void do_something () {
           this.something["my-detail"] ();

           // ...
       }
   }
