Properties
==========

Properties are very similar to fields. With them you can put data into an object.
They are usually public.

The difference is that you can have more fine-grained access controls,
and also code running on assignments or other accesses.

How to create a property
------------------------

For example in a class:

.. code-block:: vala

   public class MyClass : Object {
       public string name { get; set; }

       // ...
   }

This was a property named "name" of the type **string**.
Then in the brackets the getter and setter access modifier follow.
In this case a normal **get** and **set** is a shortcut for **public get** and **public set**.

You can also make them private or protected: ``public string name { get; private set; }``
So that you can restrict access to either getting or setting the property.

Another option is to insert code into the set and get methods:

.. code-block:: vala

   public class MyClass : Object {
       public string name { get {
           return this._name;
       } set {
           this._name = value;
           stdout.printf ("new value "+this._name);
       } }

       private string _name;

       // ...
   }

You can have just like in methods the keyword **this**.
And for the getter the value needs to be returned, while in the setter,
it is stored in a special variable named **value**.

Default values
--------------

You can also let a property have a default value, that it will have from the intialisiation.
This works only with Classes.

.. code-block:: vala

   public class MyClass : Object {
       public string name { get; set; default = "Unkown"; }

       // ...
   }

Static and class properties
---------------------------

Like static fields, static properties hold the same value for every instance of a class or subclasses of it.

**class** properties are similar to **class** fields, in that they have the same value for every instance of a class, but not subclasses of it.
Subclasses can override the value with their own.

.. code-block::

   public class MyClass : Object {
        public class int n { get; set; }

        class construct {
            n = 0;
        }
   }

   public class AnotherClass : MyClass {
        class construct {
            n = 1000;
        }
   }

Construct properties
--------------------

Classes derived from **GLib.Object** have another special concept called construct properties.

They are declared like this:

.. code-block:: vala

   public class MyClass : Object {
       public string name { get; construct set; }

       construct {
           stdout.printf (this.name);
       }

       public MyClass (string name) {
           Object (name: name);
       }

       // ...
   }

Construct properties are set upon construction of the object.
So that in the construct block you can access them.
Either they are provided by the user who constructs the object, or the default values are taken.

To make it more convenient you will mostly have the constructor that calls **Object ()**
with the right properties, either from parameters or hard-coded.

But also normal properties that have public set methods, can be initialised with values from the constructor like construct properties.
They are just not required to be set, so you also cannot access them from the construct block.

Construct-only properties
~~~~~~~~~~~~~~~~~~~~~~~~~

The construct property with ``{ get; construct set; }`` can be also later accessed and modified.
You can also make it only once settable on construction and then never again:

.. code-block:: vala

   public class MyClass : Object {
       public string name { get; construct; }

       // ...
   }

This type of property is called "construct-only" property.
Of course also a default value can be provided, that is used, when the property wasn't initialised by the user.
