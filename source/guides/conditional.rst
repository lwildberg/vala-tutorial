Conditional Execution
=====================

Until now, your program can only execute multiple steps after each other, and then stops.
But you can also let your application leave out some code, or execute some other only in specific circumstances.

Conditions
----------

First you need to know what a condition is.
Conditions are expressions, that return a boolean value, ``true`` or ``false``.

Relational Operators
~~~~~~~~~~~~~~~~~~~~

For example you can compare two values, and if they are equal, return ``true``:

.. code-block:: vala

   foo == 10

There are also other available operators for conditions:

.. code-block:: vala

   foo > 10 // the number foo must be greater than 10

   foo < 10 // foo must be smaller than 10 to return true

   foo >= 10 // foo needs to be greater than or equal to 10. the same as foo > 9

   foo <= 10

   foo != 10 // returns true only if foo not contains 10

Of course you can also just use a boolean variable,
a function that returns a boolean value as a condition,
or even ``true`` or ``false`` directly.

Logical Operators
~~~~~~~~~~~~~~~~~

Sometimes it is useful to combine two conditions,
for example if you want that a variable to be between 10 and 20.

This can be accomplished with logical operators:

.. code-block:: vala

   number <= 20 && number >= 10

`And` (``&&``) returns only ``true``, if both expressions on the left and right side are ``true``.

Logical `Or` (``||``) instead needs only one ``true`` expression to also return ``true``:

.. code-block:: vala

   // returns true when foo is smaller
   // than 20 or when it is exactly 30
   foo < 20 || foo == 30

A third operator is ``!``, which simply turns ``true`` into ``false`` and vice-versa:

.. code-block:: vala

   bool condition = !(5 > 6) // true

You can combine these operations further to build more complex conditions.

If
--

Sometimes you have code that should only run, when a specific condidtion is met.
Do that with the ``if`` statement:

.. code-block:: vala

   if (condition) {
       // here is your code
   }

An example would be:

.. code-block:: vala

   if (foo == 10) {
       stdout.printf ("foo is 10\n");
   }

In this case only when the variable ``foo`` exactly contains the number ``10``, the program prints out "foo is 10".

In the example ``foo == 10`` is a condition.

Else
----

You can also execute code whenever an ``if``-statement was `not` executed:

.. code-block:: vala

   if (foo == 10) {
       stdout.printf ("foo is 10\n");
   } else {
       stdout.printf ("foo is not 10\n");
   }

It covers all other remaining cases, when ``foo`` is for example 11, 12, 9, etc. but not 10.

Else If
-------

To further extend that concept, you can also specify alternative code sections, with a different condition,
that get checked and run when the condition in ``if`` returned false:

.. code-block:: vala

   if (c == 0) {
       stdout.printf ("0\n");
   } else if (c == 1) {
       stdout.printf ("1\n");
   } else if (c == 2) {
       stdout.printf ("2\n");
   }

   stdout.printf ("done\n");

If ``c == 0`` here is ``true``, the program prints out 0. If not, the next ``else if`` is checked.
If it also not ``true``, the next one is taken etc. And when one condition is ``true``,
the corresponding code block is executed and after that the programs continues with code after the last ``else if`` and prints out "done".

Of course it can have also an additional ``else`` part at the end:

.. code-block:: vala

   if (c == 0) {
       stdout.printf ("c is 0\n");
   } else if (c == 1) {
       stdout.printf ("c is 1\n");
   } else {
       stdout.printf ("c is not 0 and also not 1\n");
   }

Switch
------

With ``switch`` the code checks if a expression equals exactly a value, and executes then code for that value:

.. code-block:: vala

   switch (fruit) {
       case "apple":
           stdout.printf ("An apple.\n");
           break;
       case "cherry":
           stdout.printf ("A cherry.\n");
           break;
       case "banana":
           stdout.printf ("A banana.\n");
           break;
       case "blueberry":
       case "raspberry":
           stdout.printf ("A berry.\n");
           break;
       default:
           stdout.printf ("This is no fruit.\n");
           break;
   }

Here it is checked first if ``fruit`` constains "apple", if yes it is printed out
and then the program exits with ``break;`` the switch statement.
If not, then it is checked for "cherry" with the same process and so on.
As you can see with "blueberry", you can also leave the ``break;`` out.
Then the program just continues to execute the following commands without caring about another ``case`` statement.

Also useful
-----------

* If the code inside of a ``if`` or ``else`` block or any other code block from a statement is only one line,
  you can avoid the curly brackets:

  .. code-block:: vala

     if (foo == 10)
         stdout.printf ("foo is 10\n");

