Vala Documentation
==================

Welcome to the documentation for the Vala programming language.
You can lookup all information about Vala here or use it as a reference for starting with Vala.

Content overview
----------------

This documentation is divided roughly into three sections:

* :doc:`Developer Guides<guides>`: In this section are all language features of vala and more useful things documented.

* :doc:`Tutorials<tutorials>`: For tutorials about how to use Vala for lots of different use cases you can find more here.

* :doc:`Tooling<tooling>`: Lookup details and more information about tooling that comes with Vala

* :doc:`Contribute to Vala<contribute>`: If you want to contribute to vala,
  in this section you can find out more about how the vala compiler works internally.

Additional Resources
--------------------

You can also find information from other places:

* `API references <https://valadoc.org>`__: Search and lookup all libraries for Vala and their functionalities.

* `Reference Manual <https://lwildberg.pages.gitlab.gnome.org/vala-reference-manual>`__: The place for technical
  specification of the syntax and other features of the Vala language

* `Wiki <https://wiki.gnome.org/Projects/vala>`__:
  Has lots of useful extra documentation about Vala, but is sometimes a bit outdated.

* `GNOME Developer Documentation <https://developer.gnome.org>`__:
  Contains documentation and tutorials about platform libraries, design, the GNOME Builder IDE and much more.

* `Vala Wesbite <https://vala.dev>`__: Contains general information and the latest news related to the language.

Get in touch
------------

If you want to talk to the developers there are multiple ways:

* `Discourse <https://discourse.gnome.org/tag/vala>`__: Ask for help in the discussion forums.

* `Matrix <https://matrix.to/#/#_gimpnet_#vala:gnome.org>`__: Have a chat with the developers and ask your questions.

Improve this Documentation
--------------------------

To contribute, go to the `gitlab repository <https://gitlab.gnome.org/lwildberg/vala-tutorial>`__.

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Contents:
   
   guides
   tutorials
   tooling
   contribute

